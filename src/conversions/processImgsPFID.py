def loadRestaurants(fn):
	restaurants = open(fn, 'r');
	restDicts = {};
	for line in restaurants :
		tokens = line.split("\t");
		restDicts[tokens[0]] = tokens[1];
	return restDicts;

def loadFood(fn):
	foods = open(fn, 'r');
	foodDicts = {};
	caloriesDicts ={};
	for line in foods:
		line = line.strip('\n');
		tokens = line.strip('\n').split("\t");
		foodDicts[tokens[0]] = tokens[1].strip('\n');
		caloriesDicts[tokens[0]] = tokens[2].strip('\n');
	return foodDicts, caloriesDicts;

def processImages(fn, dictChains, dictItems, dictCalories):
	images = open(fn, 'r');
	counts = {};
	for line in images:
		if ((len(line) == 0) or "/videos/" in line) :
			continue;
		tokens = line.split('/')	
		chain = dictChains[tokens[1]];
		if (tokens[2] not in dictItems):
			continue;
		item = dictItems[tokens[2]];
		calories = dictCalories[tokens[2]];
		if (item in counts.keys()):
			counts[item] += 1;
		else :
			counts[item] = 1;

		print( "cp '%s' ../conv/%s-%s-%s_%s.jpg" % (line.strip('\n'), chain.strip('\n'), item, counts[item], calories));

chains = loadRestaurants('./restaurants.txt');

items, calories = loadFood('./foodcal.txt');

processImages('./images.txt', chains, items, calories);
