import numpy as np

def loadLabels(labels_fn) :
        labels = open(labels_fn, 'r')
        labelDicts = {};
        m = 1;
        for line in labels:
		if (m == 569) :
			return;
                line = line.strip().split('\t');
                label = line[0].replace(" " ,"").replace("\"", "").replace("(","").replace(")","").replace("\/","");
		calories = int(float(line[1]));
		imageid = line[2];

                print("cp %s.jpg ../conv/%s_%d.jpg" % (imageid.zfill(4), label, calories));
		m += 1;

loadLabels('metadata.txt');
