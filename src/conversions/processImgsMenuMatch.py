import numpy as np

def loadLabels(labels_fn) :
        labels = open(labels_fn, 'r')
        labelDicts = {};
        m = 0;
        for line in labels:
                line = line.strip(' \t\n\r').split(';');
                m +=1;
                for i in range (len(line) - 1):
                        imagefn = line[0];
                        label = line[i+1].strip(' \t\n\r');
                        if (len(label) == 0) :
                                continue;
                        if (label in labelDicts):
                                labelDicts[label].append(imagefn);
                        else :
                                dataarr = [];
                                dataarr.append(imagefn);
                                labelDicts[label] = dataarr;
        #print 'total entries %d', m;
        return labelDicts;

def loadCalories(calories_fn) :
        itemInfo = open(calories_fn, 'r');
        caloriesDict = {};
        categoriesDict = {};
        for line in itemInfo:
                if ('Name on Menu;' in line) :
                        continue;
                line = line.strip().split(';');
                label = line[1].strip(' \t\n\r');
                calories = line[2].strip(' \t\n\r');
                category = line[3].strip(' \t\n\r');
                caloriesDict[label] = calories;
                categoriesDict[label] = category;
        return caloriesDict, categoriesDict;

[dictCalories, dictCategories] = loadCalories('./items_info.txt');
dictLabels = loadLabels('./labels.txt');
for l in dictLabels.keys():
        calories = dictCalories[l];
        category = dictCategories[l];
        fns = dictLabels[l];
        for fn in xrange(len(fns)):
                print("cp %s ../conv/%s_%d_%s_%s.jpg" % (fns[fn], category, fn, l.strip('_'), calories))
