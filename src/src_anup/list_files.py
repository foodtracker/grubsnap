import os.path
import random

def get_sets(all_fnames, set_sizes):
  random.shuffle(all_fnames)
  dset = [ [], [], [] ]
  num = 0
  for (idx , c)  in enumerate(set_sizes):
    dset[idx] =  all_fnames[num:num+set_sizes[idx]]
    num += set_sizes[idx]

  print( "tr_set_size = %s\n"% len(dset[0]))
  print( "val_set_size = %s\n"% len(dset[1]))
  print( "test_set_size = %s\n"% len(dset[2]))
  return dset[0], dset[1] , dset[2]

if __name__ == '__main__':

  path = os.path.join(os.getcwd(), "compressed_data")

  allFiles = os.listdir(path)
  m = len(allFiles)

  percent = [60, 20, 20]
  set_sizes = [(x*m)/100 for x in percent]
 
  print "There are " + str(m) + " Images" 

  tr_set, val_set, test_set = get_sets(allFiles, set_sizes)
