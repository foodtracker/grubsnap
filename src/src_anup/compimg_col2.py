from scipy import misc
import os.path
from PIL import Image, ImageOps

def compress_image(image, image_name, max_size=(200,200)):
  fname, fext = image_name.split('.')
  groups = fname.split('_')
  pre = '_'.join(groups[:2])
  
  new_name = "%s_%d_%d.%s"%(pre,max_size[0],max_size[1],fext)
  comp_image = ImageOps.fit(image, max_size, Image.ANTIALIAS)
  comp_loc = os.path.join(os.getcwd(), "compressed_data_color", new_name)
  comp_image.save(comp_loc)



if __name__ == '__main__':
    data_path = os.path.join(os.getcwd(), "color_data")
    allFiles = os.listdir(data_path)
    print ("Size of allFiles = %d"%len(allFiles))
    for fname in allFiles:
      if fname.endswith(".jpg"):
        image_loc = os.path.join(data_path,fname)
        if not os.path.isfile(image_loc):
          print "path "+image_loc+" not found"
        image = Image.open(image_loc)
        compress_image(image, fname)

  
  
  
