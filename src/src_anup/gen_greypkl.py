import sys
import cPickle, gzip, numpy
from PIL import Image 
from PIL import ImageOps
import numpy as np
from scipy.misc import imresize
import random
import os.path

"""
1. load image 
2. Resize image 
3. Flatten into numpy array
"""
def load_image( fname , size ) :
    data_path = os.path.join(os.getcwd(), "compressed_data", fname)    
    img = Image.open(data_path)
    img = img.resize(size, Image.ANTIALIAS)
    data = np.asarray( img , dtype="float32" ).flatten()
    data = (data - np.mean(data))/np.std(data)
    print("load_image size %d shape %s " % (len(data) , data.shape))
    return data

"""
assign class to filename
1. Pick each file-name  
2. split into name and calories 
3. map calories to calorie-range class  
4. Add image to the class list
"""
def assign_class( classes , fnames ):
    fname_calories = { }
    for fname in fnames:
        #split the name on 
        lw = fname.split('_')
        print(lw)
        calories = int(lw[1])
        for i, r in enumerate(classes):
            if calories >= r[0] and calories <= r[1]:
                if i not in fname_calories:
                    fname_calories[i] = []
                #read file into numpy array

                fname_calories[i].append(fname)
    return fname_calories


def class_to_dset( fname_calories , m , size  ):

    #dataset is pair of (image list , class list)
    data_x =[] 
    data_y =[] 
    for c,flist in fname_calories.items():
        for fname in flist:
            fdata  = load_image( fname , size ) 
            data_x.append(fdata)
            data_y.append(c)

    arr_data_x = np.asarray(data_x)
    arr_data_y = np.asarray(data_y)
    print(" m = %d x_shape = %s y_shape = %s"%(m , arr_data_x.shape , arr_data_y.shape) )
    data_set = ( arr_data_x , arr_data_y )
    return data_set


def dsets_to_pkl( tr_set , val_set , test_set , fname ):
    f = gzip.open(fname,'wb')
    cPickle.dump( (tr_set , val_set , test_set), f , protocol = cPickle.HIGHEST_PROTOCOL )
    f.close()
    print("Pickle dump done \n")

def verify_pkld_dsets( tr_set , val_set , test_set , fname ):
     f = gzip.open(fname, 'rb')
     tr_set_new , val_set_new , test_set_new = cPickle.load(f)
     f.close()


     return ( all(tr_set_new[0]) in tr_set[0] and
              all(tr_set_new[1]) in tr_set[1] and
              all(val_set_new[0]) in val_set[0] and
              all(val_set_new[1]) in val_set[1] and
              all(test_set_new[0]) in test_set[0] and
              all(test_set_new[1]) in test_set[1] )

def get_sets( all_fnames , set_sizes ):
    #shuffle the filenames
    random.shuffle(all_fnames)
    #split list into training , validation and error list using set_sizes
    dset = [ [] , [] , [] ]
    num = 0
    for (idx , c)  in enumerate(set_sizes):
       dset[idx] =  all_fnames[num:num+set_sizes[idx]]
       num += set_sizes[idx]

    print( "tr_set_size = %s\n"% len(dset[0]))
    print( "val_set_size = %s\n"% len(dset[1]))
    print( "test_set_size = %s\n"% len(dset[2]))
    return dset[0], dset[1] , dset[2]


def print_class_mapping( header , fname_calories ):
    print("\t\t%s\t\t"%header)
    print("\tCLASS\t\tMEMBERS")
    for k,v in fname_calories.items():
        print("\t%s\t\t%s"% ( str(k) , str(v) ))



if __name__ == '__main__':


    #Calorie ranges
    classes = [ (0,200) , (201,400) , (401, 600) , (601, 800) , (801, 1000) ]
    #Fixed image size to convert to vector
    image_size = (200,200)
    data_path = os.path.join(os.getcwd(),"compressed_data")
    allFiles = os.listdir(data_path)
    m = len(allFiles)    

    # percent of dataset in training , validation , testing
    percent  = [60 , 20 , 20]
    set_sizes = [(x*m)/100 for x in percent]
    
    tr_set,val_set,test_set = get_sets( allFiles , set_sizes)

    #Get filenames with calories TBD. randomized
    tr_fname_calories = assign_class(classes , tr_set )
    val_fname_calories = assign_class(classes , val_set )
    test_fname_calories = assign_class(classes , test_set )

    print_class_mapping("VAL" , val_fname_calories )
    print_class_mapping("TRAINING" , tr_fname_calories )
    print_class_mapping("TEST" , test_fname_calories )

    tr_data_set = class_to_dset( tr_fname_calories , set_sizes[0] , image_size )
    val_data_set = class_to_dset( val_fname_calories , set_sizes[1] , image_size )
    test_data_set = class_to_dset( test_fname_calories , set_sizes[2] , image_size )

    fname = 'grub_col_data.pkl.gz';

    dsets_to_pkl( tr_data_set , val_data_set , test_data_set , fname )

    #if verify_pkld_dsets( tr_data_set , val_data_set , test_data_set , fname ) == True:
    #   print("PICKLE SUCCESS")
    #else:
    #   print("PICKLE FAILED")

       

