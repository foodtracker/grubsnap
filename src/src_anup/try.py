import sys
import cPickle, gzip, numpy
from PIL import Image 
import numpy as np
import theano

def shared_dataset(data_xy, borrow=True):
   data_x, data_y = data_xy
   print ("shape[%s] , type[%s]\n" % ( data_x.shape , type(data_x).__name__) )
   #print( data_x[0] )

   shared_x = theano.shared(numpy.asarray(data_x, 
              dtype=theano.config.floatX),borrow=borrow) 
   shared_y = theano.shared(numpy.asarray(data_y, 
              dtype=theano.config.floatX),borrow=borrow) 


def verify_pkld_dsets( fname ):
     f = gzip.open(fname, 'rb')
     tr_set_new , val_set_new , test_set_new = cPickle.load(f)
     f.close()
     shared_dataset(tr_set_new, borrow=True)
     shared_dataset(val_set_new, borrow=True)
     shared_dataset(test_set_new, borrow=True)



fname  = 'grub_col_data.pkl.gz'
#fname  = 'mnist.pkl.gz'
verify_pkld_dsets(fname)

    


