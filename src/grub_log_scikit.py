import cPickle
import gzip
import os
import logging
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.cross_validation import train_test_split
from sklearn import metrics
from sklearn.cross_validation import cross_val_score
from sklearn.metrics import confusion_matrix
logging.basicConfig(filename='grub_scikit.log', level=logging.INFO)



def load_data( dataset ):
    new_path = os.path.join(
           os.path.split(__file__)[0],
           "..",
           "data",
           dataset
       )
    dataset = new_path
    f = gzip.open(dataset, 'rb')
    train_set, valid_set, test_set = cPickle.load(f)
    f.close()
    return train_set , valid_set , test_set


def logReg( (tr_X, tr_y) , (vl_X,vl_y) , (ts_X, ts_y) ):
    # instantiate a logistic regression model, and fit with X and y
    model = LogisticRegression()
    print ("Fitting Model on Training Data ")
    model = model.fit(tr_X, tr_y)

    print ("Testing Model on Training Data ")
    # check the accuracy on the training set
    tr_score = model.score(tr_X, tr_y)
    logging.info('Training score %f'%tr_score) 
    tr_y_pred = model.predict(tr_X)

    logging.info('Training Confusion Matrix')
    conf_tr_matrix = confusion_matrix( tr_y, tr_y_pred )
    logging.info(conf_tr_matrix)

    print ("Testing Model on Validation Data ")
    vl_score = model.score(vl_X, vl_y)
    logging.info('Validation score %f'%vl_score) 
    vl_y_pred = model.predict(vl_X)
    logging.info('Validation Confusion Matrix')
    conf_val_matrix = confusion_matrix( vl_y, vl_y_pred )
    logging.info(conf_val_matrix)
   
    print ("Testing Model on Testing Data ")
    ts_score = model.score(ts_X, ts_y)
    logging.info('Testing score %f'%ts_score) 
    ts_y_pred = model.predict(ts_X)
    conf_ts_matrix = confusion_matrix( ts_y, ts_y_pred )
    logging.info('Testing Confusion Matrix')
    logging.info(conf_ts_matrix)

if __name__ == '__main__':

    files = { 'grub_grey_100_100_data.pkl.gz': 100*100 }
    #files = { 'grub_grey_100_100_normalized_0_data.pkl.gz': 100*100 }
    #files = { 'grub_grey_100_100_ipca_normalized_1_data.pkl.gz': 100*100 }
    #files = { 'grub_grey_100_100_rpca_normalized_1_data.pkl.gz': 100*100 }
    #files = { 'grub_grey_100_100_rpca_prenorm_1_data.pkl.gz': 100*100 }
    #files = { 'grub_grey_100_100_FA_normalized_1_data.pkl.gz': 100*100 }
    #files = { 'grub_grey_100_100_rpca_prenorm_1_data.pkl.gz': 100*100 }
    #files = { 'grub_grey_ipca_prenormalized_1_100_100_data.pkl.gz': 100*100 }
    #files = { 'grub_grey_100_100_RPCA_fitXY_normalized_1_data.pkl.gz': 100*100 }
    
    for fp in files:
      print(fp)
      logging.info('Using %s'% fp) 
      train_set , valid_set , test_set  = load_data(fp)
      logReg(train_set,valid_set,test_set)




   
