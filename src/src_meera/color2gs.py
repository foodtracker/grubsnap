#!/home/ubuntu/anaconda2/bin/python
from scipy import misc
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import sys
from PIL import Image, ImageOps

#convert pixel to greyscale using weighted average of each color
def weightedAverage(pixel):
    return 0.299*pixel[0] + 0.587*pixel[1] + 0.114*pixel[2]

#convert image to greyscale using weighted average of each pixel
def convert_to_grey_scale(image):
   grey = np.zeros((image.shape[0], image.shape[1])) # init 2D numpy array
   for rownum in range(len(image)):
       for colnum in range(len(image[rownum])):
           grey[rownum][colnum] = weightedAverage(image[rownum][colnum])
   return(grey)

def compress_image(image , max_size = (720,720)):
    #return misc.imresize(image ,max_size)
    fname, fext = image.split('.')
    new_name = "%s_%d_%d.%s"%(fname,max_size[0],max_size[1],fext)
    img = Image.open(image)
    ImageOps.fit(img, max_size, Image.ANTIALIAS)
    print(new_name)
    img.save(new_name)


if __name__ == '__main__':

    for name in sys.argv[1:]:
        if '.jpg' in name:
            fname,fext = name.split('.')
            grey_fname = fname + '_grey' + '.' + fext
            image = misc.imread(name)
            gs_image = convert_to_grey_scale(image)
            misc.imsave( grey_fname , gs_image )
            compress_image(grey_fname)
            compress_image(name)
